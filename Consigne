Formateur - Thomas SAQUETSujet du projet CI/CD (sera déplacé dans le bon canal dès que possible)
    
Le projet ci-dessous vise à développer des compétences pratiques en CI/CD, en gestion de projet, et en collaboration en équipe.


# Projet CI/CD pour M2 DevOps


## Objectif Général


Le but du projet est de mettre en place et gérer des pipelines CI/CD relativement complexes en utilisant Docker, Terraform, GitLab CI/CD, Kubernetes, ArgoCD, et Helm.
Il tient compte des connaissances que vous avez acquises au cours des années précédentes sur une grande partie de ces différentes technologies.


Vous travaillerez avec des environnements locaux et de production, utilisant :
 - k3d pour le développement local
 - Azure Kubernetes Service (AKS) pour la production


Le sujet laisse des libertés à plusieurs niveaux, profitez-en pour tester des choses et faire les bons choix.


Je suis présent les 5 jours avec vous, le but est de vous guider pour vous aider à avancer, pas de vous donner les solutions directement :)


## Préparation du Projet
1. **Fork du Dépôt Initial**:
   - Créer un fork privé du dépôt GitLab : [ocf-core](https://gitlab.com/open-course-factory/ocf-core).
   - Garder le fork privé.


2. **M'ajouter comme Mainteneur**:
   - Ajouter @tsaquet comme mainteneur sur le fork privé pour suivi et évaluation.


## Durée et Organisation du Projet
- **Durée**: La semaine complète.
- **Organisation Suggérée**:
  - Jours 1: Configuration initiale et planification.
  - Jours 2-4: Développement actif.
  - Jour 5 (Matinée): Finalisation et tests.
  - Jour 5 (Après-Midi): Préparation de la soutenance et soutenance.


## Soutenance
- Présentation du projet dans la dernière demi-journée de la semaine. (à partir de 14h vendredi)


## Le projet !


Les consignes du projet, en deux parties. L'objectif est de voir deux modèles de CI/CD différents, un en mode "push" (Gitlab CI/CD), un en mode "pull" (GitOps)


### Partie 1: CI/CD avec GitLab et Kubernetes


#### Objectifs d'Apprentissage
- Configuration de l'environnement local (k3d) et de production (AKS).
  - Le but est de découvrir k3d et d'en profiter pour améliorer vos connaissances sur Kubernetes.
  - Rendre un déploiement fonctionnel sur un cluster local et distant peut présenter quelques challenges intéressants
- Développement du pipeline CI/CD avec GitLab.
  - Ici on révise le travail sur Gitlab fait l'année dernière et on le pousse plus loin en connectant Kubernetes en bout de chaîne!
- Déploiement avec Helm et intégration avec Kubernetes.
  - Une fois que le cluster est branché sur Gitlab, on peut déployer avec Helm (et découvrir ou redécouvrir cette technologie au passage)


#### Livrables
- Scripts Terraform, configuration k3d, fichiers .gitlab-ci.yml, charts Helm, documentation.


### Partie 2: CI/CD avec ArgoCD


#### Objectifs d'Apprentissage
- Configuration et utilisation d'ArgoCD.
  - Ici le but est de découvrir une autre manière de faire de la CI, on peut imaginer la base entièrement sur ArgoCD et GitOps.
- Gestion des environnements avec ArgoCD.
   - Grâce à vos deux clusters, vous pouvez gérer au moins deux environnements !
- Intégration avec Helm pour le déploiement.
   - Ici, il est fort probable qu'une grande partie du travail soit en commun avec la partie 1, soyez malins.


#### Livrables
- Configuration ArgoCD, définitions des applications ArgoCD, documentation.


### Ressources et Support
- Accès au dépôt GitLab, documentation officielle des outils.


## Évaluation


### Soutenance


La soutenance doit permettre de se rendre compte des éléments suivants, sur lesquels vous serez notés :


- Qualité technique et fonctionnalité des pipelines CI/CD.
  - Est-ce que ça fait ce qui est demandé ?
  - Est-ce que les étapes sont cohérentes, est-ce que les différents outils sont utilisés correctement, est-ce que les différentes fonctionnalités sont utilisées ?
- Qualité et clarté de la documentation.
  - Si quelqu'un est débutant avec tous ces sujets, est-ce qu'il peut démarrer le projet et le mettre sur son cluster local et en production grâce à la documentation ?
  - Est-ce que la qualité de rédaction est correcte ? (Si vous voulez une idée de à quoi ressemble une documentation de qualité, allez voir du côté de celle de Gitlab !)
- Gestion du dépôt Git et collaboration en équipe.
  - Est-ce que les fonctionnalités de travail en équipe de Gitlab sont bien utilisées ?


### Barême


1. Qualité Technique des Pipelines CI/CD (30 points)
  - Fonctionnalités (10 points) :
    - Les pipelines fonctionnent sans erreur (5 points)
    - Les différents outils demandés sont utilisés (5 points)
  - Utilisation des Outils (15 points) :
    - Docker et Kubernetes (Dockerfile multistage, Kubernetes déployé localement & sur Azure) (3 points)
    - Mise en œuvre de Terraform (Le cluster de prod est bien déployé et maintenu grâce à Terraform) (3 points)
    - Les clusters Kubernetes sont bien pilotés via Gitlab (runners & déploiements) (5 points)
    - Intégration pertinente d’ArgoCD/Helm avec le pipeline (4 points)
  - Optimisation et Sécurité (5 points) :
    - Reflexions et implémentation de mesures de sécurité (utilisation correcte des variables utilisateurs dans Gitlab par exemple) (3 points)
    - Optimisation du temps d’exécution et des ressources (2 points)
2. Qualité de la Documentation (20 points)
  - Clarté et Structure (10 points) :
    - Facilité de compréhension (5 points)
    - Organisation logique et clarté des instructions (5 points)
  - Complétude et Détails Techniques (10 points) :
    - Couverture complète de tous les aspects du projet (5 points)
     - Détails techniques et justification des choix (5 points)
3. Gestion du Dépôt Git et Collaboration (20 points)
  - Utilisation des fonctionnalités GitLab (10 points) :
    - Bonnes pratiques de gestion des branches et merge requests (5 points)
    - Utilisation des issues, labels, et milestones (5 points)
  - Dynamique d'Équipe et Collaboration (10 points) :
    - Répartition équitable des tâches (5 points)
    - Efficacité de la communication et résolution des conflits (5 points)
4. Présentation et Soutenance (30 points)
  - Qualité de la Présentation (20 points) :
    - Clarté de l’exposé et du support de présentation (choix du support libre) (10 points)
    - Capacité à maintenir l'attention et l’intérêt (5 points)
    - Utilisation efficace du temps alloué (5 points)
  - Réponses aux Questions (10 points) :
    - Pertinence et précision des réponses (5 points)
    - Capacité à justifier les choix techniques et méthodologiques (5 points)


### Participation des autres groupes à la notation


- Chaque groupe évalue les autres groupes selon les mêmes critères.
- Pour chaque critère, les groupes attribuent une note sur la base du barème.
- Les notes des autres groupes représentent 30% de la note finale.
- Les notes doivent être argumentées (il faut expliquer pourquoi la note est bonne ou mauvaise pour chaque critère)
